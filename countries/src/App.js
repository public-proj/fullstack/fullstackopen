import React, { useEffect } from 'react';
import { useState } from 'react';
import axios from 'axios';
import './App.css';
//import { getWeather } from './api';

function App() {
  const [searchTerm, setSearchTerm] = useState('');
  const [countries, setCountries] = useState([]);
  const [displayCountries, setDisplayCountries] = useState([]);
  const [singleCountry, setSingleCountry] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [weatherData, setWeatherData] = useState(null);

  useEffect(() => {
    axios.get('https://restcountries.eu/rest/v2/all').then((response) => {
      setCountries(response.data);
    });
  }, []);

  const getWeather = () => {
    if (singleCountry != null) {
      const weather_base_url = 'http://api.weatherstack.com/current';

      const params = {
        access_key: process.env.REACT_APP_WEATHER_API_KEY,
        query: `${singleCountry.latlng[0]},${singleCountry.latlng[1]}`,
      };

      axios
        .get(weather_base_url, { params })
        .then((response) => {
          console.log(response.data);
          setWeatherData(response.data.current);
          console.log('weather data', weatherData);
          setIsLoading(false);
        })
        .catch((error) => {
          setWeatherData(null);
          console.log(error);
        });
    }
  };

  useEffect(() => {
    getWeather();
  }, [singleCountry]);

  const search = (searchTerm) => {
    setSearchTerm(searchTerm);
    setSingleCountry(null);
    setIsLoading(true);
    setDisplayCountries(
      countries.filter((country) =>
        country.name.toLowerCase().includes(searchTerm.toLowerCase())
      )
    );
  };

  const weather = (country) => {
    if (weatherData != null) {
      if (isLoading) {
        return <div>Loading Weather...</div>;
      }
      return (
        <div>
          <h2>Weather in {country.name}</h2>
          temperature: {weatherData.temperature} Celcius
          <img className="h-16 w-16" src={weatherData.weather_icons}></img>
          wind: {weatherData.wind_speed} mph direction {weatherData.wind_dir}
        </div>
      );
    }
  };

  let display;

  const singleCountryContent = (country) => {
    if (country != null) {
      //const weatherData = getWeather(country.latlng[0], country.latlng[1]);

      return (
        <div>
          <h1>{country.name}</h1>
          capital: {country.capital}
          <br />
          population: {country.population}
          <h3>languages</h3>
          <ul>
            {country.languages.map((lang) => (
              <li key={lang.name}>{lang.name}</li>
            ))}
          </ul>
          <img className="w-32" src={country.flag}></img>
        </div>
      );
    }
  };

  if (displayCountries.length === 1) {
    const country = displayCountries[0];
    display = singleCountryContent(country);
  } else if (displayCountries.length > 10) {
    display = <div>Too many matches, specify another filter</div>;
  } else {
    display = (
      <div>
        <ul>
          {displayCountries.map((country) => (
            <li key={country.alpha2Code}>
              {country.name}
              <button
                className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold ml-4 my-2 px-2 hover:text-white border border-blue-500 hover:border-transparent rounded"
                onClick={() => setSingleCountry(country)}
              >
                show
              </button>
            </li>
          ))}
        </ul>
      </div>
    );
  }

  return (
    <div className="App">
      find countries
      <input
        className="ml-4 border border-blue-500"
        onChange={(e) => search(e.target.value)}
        value={searchTerm}
      />
      {display}
      {singleCountryContent(singleCountry)}
      {weather(singleCountry)}
    </div>
  );
}

export default App;
