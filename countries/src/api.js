import axios from 'axios';

const weather_base_url = 'http://api.weatherstack.com/current';

export const getWeather = (lat, lng) => {
  const params = {
    access_key: process.env.REACT_APP_WEATHER_API_KEY,
    query: `${lat},${lng}`,
  };

  return axios
    .get(weather_base_url, { params })
    .then((response) => {
      console.log(response.data);
    })
    .catch((error) => {
      console.log(error);
    });
};
