# Weather does not render when there is only one search result
too many re-renders error comes up when setSingleCountry is used in this case
```
if (displayCountries.length === 1) {
    const country = displayCountries[0];
    setSingleCountry(country) //this line gives too many re-render errors
    display = singleCountryContent(country);
  } 
```

# Most likely an API free account limitation 
subsequent api will fail, but works again after a few mins