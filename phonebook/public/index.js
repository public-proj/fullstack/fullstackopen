const find_person_by_id = () => {
  const id = document.getElementById('find-person-id').value;
  location.href = `/api/persons/${id}`;
};

const delete_person_by_id = () => {
  const id = document.getElementById('delete-person-id').value;
  fetch('/api/persons/' + id, {
    method: 'DELETE',
  }).then((location.href = `/api/persons/`)); // or res.json()
};

document
  .getElementById('find-person-id-button')
  .addEventListener('click', find_person_by_id);

document
  .getElementById('delete-person-id-button')
  .addEventListener('click', delete_person_by_id);
