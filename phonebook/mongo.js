const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

const dbUser = process.env.DBUSER;
const dbPass = process.env.DBPASS;
const dbName = process.env.DBNAME;

const connString = `mongodb+srv://${dbUser}:${dbPass}@fullstackopen.koxae.mongodb.net/${dbName}>?retryWrites=true&w=majority`;
mongoose.connect(connString);
console.log(process.env.MONGODB_URL);

const PersonSchema = new mongoose.Schema({
  name: String,
  number: String,
});

const Person = mongoose.model('Person', PersonSchema);

const person = new Person({
  name: process.argv[2],
  number: process.argv[3],
});

if (process.argv.length == 4) {
  person.save().then((result) => {
    console.log(`added ${person.name} ${person.number} to phonebook`);
    mongoose.connection.close();
  });
}

console.log('phonebook:');
Person.find({}).then((result) => {
  result.forEach((person) => {
    console.log(`${person.name} ${person.number}`);
  });
  mongoose.connection.close();
});
