const express = require('express');
const app = express();
require('dotenv').config();
const Person = require('./models/person');

const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

/* app setup */
app.use(cors());
app.use(express.json());
// old static html
// app.use(express.static('public'));
// react frontend static build
app.use(express.static('build'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  morgan(':method :url :status :req[content-length] - :response-time ms :body')
);

morgan.token('body', function (req, res) {
  return JSON.stringify(req.body);
});

/* Routes */

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/info', (request, response) => {
  Person.countDocuments({}, (error, count) => {
    if (error) {
      console.log('count error');
    } else {
      response.write(`Phonebook has info for ${count} people\n`);
      response.write(new Date().toUTCString());
      response.end();
    }
  });
});

app.get('/api/persons', (request, response) => {
  Person.find({}).then((phonebook) => {
    response.json(phonebook);
  });
});

app.get('/api/persons/:id', (request, response, next) => {
  Person.findById(request.params.id)
    .then((p) => {
      if (p) {
        response.json(p);
      } else {
        response.status(404).end();
      }
    })
    .catch((error) => {
      next(error);
    });
});

app.delete('/api/persons/:id', (request, response, next) => {
  Person.findByIdAndRemove(request.params.id)
    .then((result) => {
      response.status(204).end();
    })
    .catch((error) => next(error));
});

// add new person
app.post('/api/persons', (request, response, next) => {
  const name = request.body.name;
  const number = request.body.number;

  const phonebookEntry = new Person({
    name,
    number,
  });

  phonebookEntry
    .save()
    .then((savedEntry) => response.json({ added: savedEntry }))
    .catch((error) => next(error));
});

// update existing person
app.put('/api/persons/:id', (request, response, next) => {
  const updatedPerson = {
    number: request.body.number,
  };
  Person.findByIdAndUpdate(request.params.id, updatedPerson)
    .then((result) => {
      response.json(result);
    })
    .catch((error) => next(error));
});

const errorHandler = (error, request, response, next) => {
  console.error(error.message);

  if (error.name === 'CastError' && error.kind == 'ObjectId') {
    return response.status(400).send({ error: 'malformatted id' });
  } else if (error.name === 'ValidationError') {
    return response.status(400).send({ error: error.message });
  }
};

app.use(errorHandler);

app.listen(process.env.PORT || 3007, () => {
  console.log(`Server running on port ${process.env.PORT}`);
});
