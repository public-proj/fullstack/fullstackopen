const dotenv = require('dotenv');
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

dotenv.config();

const dbUser = process.env.DBUSER;
const dbPass = process.env.DBPASS;
const dbName = process.env.DBNAME;

const connString = `mongodb+srv://${dbUser}:${dbPass}@fullstackopen.koxae.mongodb.net/${dbName}>?retryWrites=true&w=majority`;

mongoose
  .connect(connString)
  .then(() => {
    console.log('connected to MongoDB');
  })
  .catch((error) => {
    console.log(`error connecting to MongoDB: ${error.message}`);
  });

const PersonSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    minlength: [3, 'Minimum 3 characters'],
  },
  number: {
    type: String,
    required: true,
    minlength: [8, 'Minimum 8 digits'],
  },
});

PersonSchema.plugin(uniqueValidator);

PersonSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});

//const Person = mongoose.model('Person', PersonSchema);

module.exports = mongoose.model('Person', PersonSchema);
