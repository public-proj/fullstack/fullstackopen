//import config from '../../src/utils/config';

Cypress.env();
Cypress.env('REACT_APP_BACKEND_PORT');

const backendBaseURL = `http://localhost:${Cypress.env(
  'REACT_APP_BACKEND_PORT'
)}`;
const frontendBaseURl = `http://localhost:3006/`;

// cypress custom commands
Cypress.Commands.add('login', ({ username, password }) => {
  cy.request('POST', `${backendBaseURL}/api/login/`, {
    username,
    password,
  }).then((response) => {
    localStorage.setItem('loggedBlogAppUser', JSON.stringify(response.body));
    cy.visit(frontendBaseURl);
  });
});

Cypress.Commands.add('createBlog', ({ title, author, url, likes }) => {
  cy.request({
    url: `${backendBaseURL}/api/blogs/`,
    method: 'POST',
    body: {
      title,
      author,
      url,
      likes,
    },
    headers: {
      Authorization: `bearer ${
        JSON.parse(localStorage.getItem('loggedBlogAppUser')).token
      }`,
    },
  });
  cy.visit(frontendBaseURl);
});

// Tests
describe('Blog app', function () {
  beforeEach(function () {
    //TODO: use the port variable in config
    cy.request('POST', `${backendBaseURL}/api/testing/reset`);
    // create a new user
    const user = {
      username: 'mluukkai',
      name: 'Matti Luukkainen',
      password: 'salainen',
    };
    cy.request('POST', `${backendBaseURL}/api/users/`, user);
    cy.visit(frontendBaseURl);
  });

  it('Login form is shown', function () {
    cy.contains('Log In');
    cy.contains('username');
    cy.contains('password');
    cy.contains('login');
  });

  // 5.18
  describe('Login', function () {
    it('succeeds with correct credentials', function () {
      cy.contains('Log In').click();
      cy.get('#username').type('mluukkai');
      cy.get('#password').type('salainen');
      cy.contains('login').click();
      cy.contains('Matti Luukkainen logged-in');
    });
    it('fails with wrong credentials', function () {
      cy.contains('Log In').click();
      cy.get('#username').type('wrong');
      cy.get('#password').type('password');
      cy.contains('login').click();
      cy.contains('Wrong username or password');
      // check text color is red
      cy.get('#notification').should('have.css', 'color', 'rgb(114, 28, 36)');
    });
  });

  // 5.19
  describe('Blog app', function () {
    describe('When logged in', function () {
      beforeEach(function () {
        // login the user here
        // Cypress recommendation - Fully test the login flow – but only once!
        // so using an http request to log in here
        cy.login({ username: 'mluukkai', password: 'salainen' });
      });

      it('A blog can be created', function () {
        cy.contains('Create New Blog').click();
        cy.get('#title').type('Cypress Test Blog');
        cy.get('#author').type('Cypress Blog Author');
        cy.get('#url').type('http://www.cypressblog.com');
        cy.contains('Save').click();
        cy.contains('Cypress Test Blog');
      });

      /*
        cy.createBlog({
            title: 'Cypress Test Blog Title',
            author: 'Blog Author',
            url: 'http://www.blogurl.com',
            likes: 4,
        });        
        */
      // 5.20
      describe('and several blogs exist', function () {
        beforeEach(function () {
          cy.createBlog({
            title: 'first blog',
            author: 'first author',
            url: 'http://www.first.com',
          });
          cy.createBlog({
            title: 'second blog',
            author: 'second author',
            url: 'http://www.second.com',
          });
          cy.createBlog({
            title: 'third blog',
            author: 'third author',
            url: 'http://www.third.com',
          });
        });

        it('one of those blogs can be liked', function () {
          cy.contains('second blog')
            .contains('view')
            .click()
            .parent()
            .parent()
            .as('singleBlogRoot')
            .find('.like-button')
            .click();

          cy.get('@singleBlogRoot').find('.blog-likes').should('contain', '1');
        });
      });

      // 5.21
      describe('blog deletion', function () {
        beforeEach(function () {
          cy.createBlog({
            title: 'first blog',
            author: 'first author',
            url: 'http://www.first.com',
          });
        });

        it('blog can be deleted by owner', function () {
          cy.contains('first blog')
            .contains('view')
            .click()
            .parent()
            .parent()
            .as('singleBlogRoot')
            .find('.delete-button')
            .click();

          cy.contains('first blog').should('not.exist');
        });

        it('blog cannot be deleted by non owner', function () {
          // create a second user
          const user = {
            username: 'secondUser',
            name: 'Second User',
            password: 'second',
          };
          cy.request('POST', `${backendBaseURL}/api/users/`, user);

          // logout and log in with new user
          cy.contains('Logout').click();
          cy.login({ username: 'secondUser', password: 'second' });

          // should not see a delete button
          cy.contains('first blog')
            .contains('view')
            .click()
            .parent()
            .parent()
            .should('not.have.class', 'delete-button');
        });
      });

      // 5.22
      describe('blogs sorting', function () {
        beforeEach(function () {
          cy.createBlog({
            title: 'first blog',
            author: 'first author',
            url: 'http://www.first.com',
          });
          cy.createBlog({
            title: 'second blog',
            author: 'second author',
            url: 'http://www.second.com',
          });
          cy.createBlog({
            title: 'third blog',
            author: 'third author',
            url: 'http://www.third.com',
          });
        });

        it('blogs sorted by number of likes', function () {
          cy.contains('second blog')
            .contains('view')
            .click()
            .parent()
            .parent()
            .find('.like-button')
            .as('like-button-2')
            .click();
          cy.get('@like-button-2').click();

          cy.contains('third blog')
            .contains('view')
            .click()
            .parent()
            .parent()
            .find('.like-button')
            .click();

          // order should be blog 2 (2 likes) -> blog 3 (1 like) -> blog 1
          const blogsOrder = ['second blog', 'third blog', 'first blog'];
          cy.get('.blog-title').then((blogs) => {
            blogs.map((index, blog) => {
              expect(blog.childNodes[0].nodeValue).to.eq(blogsOrder[index]);
            });
          });
        });
      });
    });
  });
});
