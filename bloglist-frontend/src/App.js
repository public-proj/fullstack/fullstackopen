import React, { useState, useEffect, useRef } from 'react';
import Blog from './components/Blog';
import BlogForm from './components/BlogForm';
import LoginForm from './components/LoginForm';
import Notification from './components/Notification';
import Togglable from './components/Togglable';
import blogService from './services/blogs';
import loginService from './services/login';

const App = () => {
  const [blogs, setBlogs] = useState([]);
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [message, setMessage] = useState({ message: null, type: 'success' });
  // const [newBlog, setNewBlog] = useState({ title: '', author: '', url: '' });

  const blogFormRef = useRef();

  useEffect(() => {
    blogService.getAll().then((blogs) => setBlogs(blogs));
  }, []);

  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem('loggedBlogAppUser');
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON);
      setUser(user);
      blogService.setToken(user.token);
    }
  }, []);

  const handleLogout = (event) => {
    event.preventDefault();
    window.localStorage.removeItem('loggedBlogAppUser');
    blogService.setToken(null);
    window.location.reload();
  };

  const handleLogin = async (event) => {
    event.preventDefault();
    // console.log('logging in with', username, password);

    try {
      const user = await loginService.login({ username, password });
      window.localStorage.setItem('loggedBlogAppUser', JSON.stringify(user));
      blogService.setToken(user.token);
      setUser(user);
      setUserName('');
      setPassword('');
    } catch (exception) {
      setMessage({ message: 'Wrong username or password', type: 'danger' });
    }
  };

  const handleLikeBlog = async (id) => {
    const updatedBlog = await blogService.likeBlog(id);
    setBlogs(
      blogs.map((b) =>
        b.id === updatedBlog.id ? { ...b, likes: updatedBlog.likes } : b
      )
    );
  };

  const handleDeleteBlog = async (id, title, author) => {
    if (window.confirm(`Remove blog ${title} by ${author}`)) {
      try {
        await blogService.deleteBlog(id);
        setMessage('Blog Deleted');
      } catch (exception) {
        setMessage({
          message: `Cannot delete blog: ${exception}`,
          type: 'danger',
        });
      }

      setBlogs(blogs.filter((b) => b.id !== id));
    }
  };

  const loginForm = () => {
    return (
      <Togglable buttonLabel="Log In">
        <LoginForm
          username={username}
          password={password}
          handleUsernameChange={({ target }) => setUserName(target.value)}
          handlePasswordChange={({ target }) => setPassword(target.value)}
          handleSubmit={handleLogin}
        />
      </Togglable>
    );
  };

  const createBlog = async (blogObject) => {
    try {
      const savedBlog = await blogService.postBlog(blogObject);
      setBlogs(blogs.concat({ ...savedBlog, user: { id: savedBlog.user } }));
      setMessage({
        message: `A new blog ${savedBlog.title} by ${savedBlog.author} added`,
        type: 'success',
      });
      blogFormRef.current.toggleVisibility();
    } catch (exception) {
      setMessage({ message: `Cannot save blog: ${exception}`, type: 'danger' });
    }
  };

  return (
    <div>
      <h2>blogs</h2>

      <Notification
        message={message.message}
        type={message.type}
        dismissMessage={() => setMessage({ message: null, type: 'success' })}
      />
      {user === null ? (
        loginForm()
      ) : (
        <div>
          <p>
            {user.name} logged-in{' '}
            <button className="btn btn-link" onClick={handleLogout}>
              Logout
            </button>
          </p>
          <Togglable buttonLabel="Create New Blog" ref={blogFormRef}>
            <BlogForm createBlog={createBlog} />
          </Togglable>
          {blogs
            .sort((a, b) => b.likes - a.likes)
            .map((blog) => (
              <Blog
                key={blog.id}
                blog={blog}
                currentUserId={user.id}
                handleLike={handleLikeBlog}
                handleDelete={handleDeleteBlog}
              />
            ))}
        </div>
      )}
    </div>
  );
};

export default App;
