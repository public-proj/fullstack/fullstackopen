require('dotenv').config();

console.log(process.env.REACT_APP_BACKEND_PORT)
const API_BASEURL = `http://localhost:${process.env.REACT_APP_BACKEND_PORT}`;
//const API_BASEURL = 'https://fso-bloglist.cherylm.repl.co';

console.log(API_BASEURL)

module.exports = {
  API_BASEURL,
};
