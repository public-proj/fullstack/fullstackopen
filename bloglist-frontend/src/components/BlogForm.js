import React, { useState } from 'react';

const BlogForm = ({ createBlog }) => {
  const [newBlog, setNewBlog] = useState({ title: '', author: '', url: '' });

  const handleBlogChange = (event) => {
    setNewBlog({ ...newBlog, [event.target.name]: event.target.value });
  };

  const handleBlogSubmit = async (event) => {
    event.preventDefault();

    createBlog(newBlog);
    setNewBlog({ title: '', author: '', url: '' });
  };

  return (
    <div className="blogFormDiv">
      <form onSubmit={handleBlogSubmit} className="form-group">
        <div className="form-group row">
          <label className="col-sm-2 col-form-label col-form-label-lg">
            Title
          </label>
          <input
            value={newBlog.title}
            className="form-control col-sm-6"
            id="title"
            name="title"
            onChange={handleBlogChange}
          />
        </div>
        <br />
        <div className="form-group row">
          <label className="col-sm-2 col-form-label col-form-label-lg">
            Author
          </label>
          <input
            value={newBlog.author}
            className="form-control col-sm-6"
            id="author"
            name="author"
            onChange={handleBlogChange}
          />
        </div>
        <br />
        <div className="form-group row">
          <label className="col-sm-2 col-form-label col-form-label-lg">
            URL
          </label>
          <input
            value={newBlog.url}
            className="form-control col-sm-6"
            id="url"
            name="url"
            onChange={handleBlogChange}
          />
        </div>
        <br />
        <button type="submit" className="btn btn-primary">
          Save
        </button>
      </form>
    </div>
  );
};

export default BlogForm;
