import React, { useState } from 'react';

const Blog = ({ blog, currentUserId, handleLike, handleDelete }) => {
  const [visible, setVisible] = useState(false);

  //const hideWhenVisible = { display: visible ? 'none' : '' };
  const showWhenVisible = { display: visible ? '' : 'none' };

  const blogStyle = {
    paddingTop: 10,
    paddingLeft: 2,
    border: 'solid',
    borderWidth: 1,
    marginBottom: 5,
  };

  const toggleVisibility = () => {
    setVisible(!visible);
  };

  const renderDeleteButton = () => {
    if (currentUserId === blog.user.id) {
      return (
        <button
          className="delete-button btn btn-outline-danger"
          onClick={() => handleDelete(blog.id, blog.title, blog.author)}
        >
          Delete
        </button>
      );
    }
  };

  return (
    <div style={blogStyle} className="blog">
      <div className="blog-title">
        {blog.title}
        <button className="btn btn-outline-primary" onClick={toggleVisibility}>
          {visible ? 'hide' : 'view'}
        </button>
      </div>
      <div style={showWhenVisible} className="togglableContent">
        <div className="blog-url">{blog.url}</div>
        <div className="blog-likes">
          {blog.likes}
          <button
            className="like-button btn btn-outline-success"
            onClick={() => handleLike(blog.id)}
          >
            like
          </button>
          {renderDeleteButton()}
        </div>
        <div>{blog.author}</div>
        {}
      </div>
    </div>
  );
};

export default Blog;
