import React from 'react';

const Notification = ({ message, type, dismissMessage }) => {
  if (message === null) {
    return null;
  }
  return (
    <div className={`d-flex justify-content-between alert alert-${type}`}>
      <div id="notification">{message}</div>
      <button className={`btn btn-outline-${type}`} onClick={dismissMessage}>
        X
      </button>
    </div>
  );
};

export default Notification;
