import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { ADD_AUTHOR_BIRTHYEAR } from '../queries';

const Authors = (props) => {
  const authors = props.authors;
  const [selectedAuthor, setSelectedAuthor] = useState();
  const [birthYear, setBirthYear] = useState();

  const [addAuthorBirthYear] = useMutation(ADD_AUTHOR_BIRTHYEAR);

  if (!props.show) {
    return null;
  }

  const handleAuthorDropBoxChange = (e) => {
    setSelectedAuthor(authors.find((author) => author.name === e.target.value));
  };

  const handleSubmit = async () => {
    if (selectedAuthor) {
      await addAuthorBirthYear({
        variables: { name: selectedAuthor.name, born: Number(birthYear) },
      });
      props.refetch();
      setBirthYear('');
    }
  };

  return (
    <div>
      <h2>authors</h2>
      <table>
        <tbody>
          <tr>
            <th></th>
            <th>born</th>
            <th>books</th>
          </tr>
          {authors.map((a) => (
            <tr key={a.name}>
              <td>{a.name}</td>
              <td>{a.born}</td>
              <td>{a.bookCount}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <h2>Set birthyear</h2>
      <select onChange={handleAuthorDropBoxChange}>
        <option>---Select Author---</option>
        {authors
          .filter((author) => author.born === null)
          .map((author) => (
            <option key={author.name} value={author.name}>
              {author.name}
            </option>
          ))}
      </select>
      <br />
      <label>born</label>
      <input
        maxLength="4"
        value={birthYear}
        onChange={(e) => setBirthYear(e.target.value.replace(/\D/, ''))}
      ></input>
      <br />
      <button onClick={handleSubmit}>update author</button>
    </div>
  );
};

export default Authors;
