import { useLazyQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import { ALL_BOOKS } from '../queries';

const getListOfGenres = (books) => {
  return [
    ...new Set(
      books
        .filter((book) => book.genres !== undefined)
        .map((book) => book.genres)
        .flat()
    ),
  ];
};

const Books = (props) => {
  const [genre, setGenre] = useState(null);

  // filter with gql queries 8.21
  const [getBooks, { loading, error, data }] = useLazyQuery(ALL_BOOKS, {
    variables: { genre },
  });

  useEffect(() => {
    getBooks();
  }, [genre, getBooks]);

  if (loading) return <p>Loading...</p>;
  if (error) return `Error! ${error}`;

  /* filter with react
  // change data.allBooks?.map to filteredBooks?.map

  const filteredBooks = genre
    ? props.books.filter((book) => book.genres.includes(genre))
    : props.books;
 */
  if (!props.show) {
    return null;
  }

  return (
    <div>
      <h2>books</h2>
      {genre !== null ? (
        <p>
          in genre <b>{genre}</b>
        </p>
      ) : null}

      <table className="table">
        <tbody>
          <tr>
            <th></th>
            <th>author</th>
            <th>published</th>
          </tr>
          {data?.allBooks?.map((book) => (
            <tr key={book.id}>
              <td>{book.title}</td>
              <td>{book.author.name}</td>
              <td>{book.published}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <h2>List of Genres</h2>
      <ul>
        {getListOfGenres(props.books).map((g) => (
          <button onClick={() => setGenre(g)} key={g}>
            {g}
          </button>
        ))}
        <button onClick={() => setGenre(null)}>all genres</button>
      </ul>
    </div>
  );
};

export default Books;
