## Refetch queries from parent component after a mutation

pass refetch from props

```js
const authorResults = useQuery(ALL_AUTHORS);

<Authors
  show={page === 'authors'}
  authors={authorResults.data.allAuthors}
  refetch={authorResults.refetch}
/>;

addAuthorBirthYear({
  variables: { name: selectedAuthor.name, born: Number(birthYear) },
});
props.refetch();
```

## log values after setting a state with useState

it will show undefined if console.log is right after setSelectedAuthor, since it will need a re-render

```js
useEffect(() => {
  console.log(selectedAuthor);
}, [selectedAuthor]);
```

# vscode REST client environmental variables

https://youtu.be/RcxvrhQKv8I?t=337
https://marketplace.visualstudio.com/items?itemName=humao.rest-client#environment-variables

# Why is login a mutation?

https://stackoverflow.com/questions/50189364/shouldnt-the-login-be-a-query-in-graphql

# Don't forget async/await

```js
const handleSubmit = async () => {
  if (selectedAuthor) {
    await addAuthorBirthYear({
      variables: { name: selectedAuthor.name, born: Number(birthYear) },
    });
    props.refetch();
    setBirthYear('');
  }
};
```

forgot to put await and refetched before the editAuthor was completed, UI did not reflect the update

# UseEffect may not run with a new prop is received
