#1 concat() does not concat in place

```
authors = authors.concat({ name: args.author, id: uuid() });
```

#2 allAuthors - async in map with mongoose

```js
allAuthors: async () => {
  const authors = await Author.find({}).lean();

  const authorsWithBookCounts = await Promise.all(
    authors.map(async (author) => {
      const count = await Book.find({
        author: author._id,
      }).countDocuments();
      return {
        ...author,
        bookCount: count,
      };
    })
  );
  return authorsWithBookCounts;
};
```

https://stackoverflow.com/questions/56043858/promise-all-doesnt-return-actual-results

- lean() was required otherwise mongoose will return an object with extra stuff
- Promise.all was needed, otherwise an array of pending promises would be returned

#3

`await Author.findOne({ name: author }).select('_id');`

`await Author.findOne({ name: author })._id;` does not work, it returns undefined.

#4
`const author = await Author.findOne({ name: args.author });` did not work. args needed to be deconstructed first

#5
Context is the right place to do things which are shared by multiple resolvers, like user identification.

#6

```js
me: (_, __, context) => {
    return context.currentUser;
},
```

cannot use \_ again for the second unused parameters

#7 Apollo3 Subscription
https://www.apollographql.com/docs/apollo-server/data/subscriptions/
https://www.apollographql.com/docs/react/data/subscriptions/
