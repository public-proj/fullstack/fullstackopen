import React from 'react';

const AddPerson = ({
  name,
  number,
  onNameChange,
  onNumberChange,
  onSubmit,
}) => {
  return (
    <form onSubmit={onSubmit} className="m-2 mb-16">
      <input
        name="name"
        className="shadow appearance-none border rounded w-40 py-2 px-3 mr-8 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        placeholder="name"
        onChange={(e) => onNameChange(e.target.value)}
        value={name}
        required
      ></input>
      <input
        name="number"
        type="tel"
        className="shadow appearance-none border rounded w-40 py-2 px-3 mr-8 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        placeholder="number"
        onChange={(e) => onNumberChange(e.target.value)}
        value={number}
        required
        pattern="[0-9\(\)\-\s]{4,}"
        title="Only numbers (, ), - and spaces are allowed"
      ></input>
      <button
        type="submit"
        className="bg-transparent hover:bg-orange-300 border-orange-300 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-orange-300 hover:border-transparent rounded"
      >
        add
      </button>
    </form>
  );
};

export default AddPerson;
