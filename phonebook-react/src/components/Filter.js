import React from 'react';

const Filter = ({ filter, handleFilterChange }) => {
  return (
    <div className="mt-4">
      Filter phonebook:{' '}
      <input
        className="shadow appearance-none border rounded w-40 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        onChange={(e) => handleFilterChange(e.target.value)}
        value={filter}
      />
    </div>
  );
};

export default Filter;
