import React from 'react';

const Notification = ({ message, dismissMessage }) => {
  if (message === null) {
    return null;
  }

  return (
    <div className="border-2 border-orange-500 text-orange-500 bg-orange-100 rounded-lg p-2 pl-4 mb-4">
      {message}
      <button className="ml-8" onClick={dismissMessage}>
        [x]
      </button>
    </div>
  );
};

export default Notification;
