import React from 'react';

const Person = ({ name, number, deleteHandler }) => {
  return (
    <>
      <li>
        {name} {number}
        <button
          onClick={deleteHandler}
          class="bg-transparent hover:bg-orange-300 border-orange-300 text-orange-500 font-semibold hover:text-white ml-2 mb-2 px-4 border border-orange-300 hover:border-transparent rounded"
        >
          Delete
        </button>
      </li>
    </>
  );
};

export default Person;
