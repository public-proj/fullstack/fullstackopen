import React, { useEffect, useState } from 'react';
import Person from './components/Person';
import Filter from './components/Filter';
import AddPerson from './components/AddPerson';
import Notification from './components/Notification';
import phonebookService from './services/phonebook';

const App = () => {
  const [persons, setPersons] = useState([]);
  const [newName, setNewName] = useState('');
  const [newNumber, setNewNumber] = useState('');
  const [filter, setFilter] = useState('');
  const [message, setMessage] = useState(null);

  useEffect(() => {
    phonebookService.getAll().then((response) => {
      setPersons(response.data);
    });
  }, []);
  //console.log('render', persons.length, 'persons');

  const addPerson = (event) => {
    event.preventDefault();
    const phonebookObject = {
      name: newName,
      number: newNumber,
    };

    if (persons.filter((entry) => entry.name === newName).length === 0) {
      phonebookService
        .create(phonebookObject)
        .then((response) => {
          // add to local copy
          setPersons(persons.concat(response.data.added));
        })
        .catch((error) => {
          setMessage(`ERROR: ${error.response.data.error}`);
        });
    } else {
      // find id of the record
      let person = persons.find((p) => p.name == newName);

      if (
        window.confirm(
          `${newName} is already in the phonebook, replace the old number with a new one?`
        )
      ) {
        phonebookService
          .update(person.id, phonebookObject)
          .then((response) => {
            // update local copy
            setPersons(
              persons.map((p) => (p.id !== person.id ? p : phonebookObject))
            );
          })
          .catch((error) => {
            setMessage(
              `The person ${person.name} has already been deleted from the phonebook. A new entry has been added with the new details`
            );
          });
      }
    }

    setNewName('');
    setNewNumber('');
  };

  const deletePerson = (id) => {
    let person = persons.find((p) => p.id == id);
    if (!window.confirm(`Remove *${person.name}* from the phonebook?`)) return;
    phonebookService
      .deleteEntry(id)
      .then(() => {
        setMessage(`*${person.name}* removed from the phonebook.`);
        setPersons(persons.filter((p) => p.id !== id)); //update local copy and re-render
      })
      .catch((error) => {});
  };

  return (
    <div>
      <h2 className="bg-orange-200 text-center text-orange-700 pt-5 h-16">
        Phonebook
      </h2>
      <div className="m-4">
        <Filter filter={filter} handleFilterChange={setFilter} />
        <AddPerson
          name={newName}
          number={newNumber}
          onNameChange={setNewName}
          onNumberChange={setNewNumber}
          onSubmit={addPerson}
        />
        <Notification
          message={message}
          dismissMessage={() => setMessage(null)}
        />

        <h2 className="text-orange-500 text-2xl mb-2">Numbers</h2>
        <ul>
          {persons.map((person) =>
            person.name.toLowerCase().includes(filter.toLowerCase()) ? (
              <Person
                name={person.name}
                number={person.number}
                deleteHandler={() => deletePerson(person.id)}
              />
            ) : (
              []
            )
          )}
        </ul>
      </div>
    </div>
  );
};

export default App;
