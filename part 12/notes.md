# Some Commands

`docker run -it --rm -v <SOURCE_PATH>:/app <IMAGE_NAME>`

`docker run -it --rm -p 3000:3000 -v "$(pwd):/usr/src/app/" hello-front-dev`

`docker exec -it bf bash`

`docker exec hello-front-dev npm install axios`

# Ex 12.16 add backend server to backend docker-compose.dev.yml

ENV variable changes - `MONGO_URL=mongodb://the_username:the_password@localhost:3456/the_database`
to `MONGO_URL=mongodb://the_username:the_password@mongo:27017/the_database`

1. localhost -> mongo
2. 3456 -> 27017 (external port -> internal port)

add

```
build:
      context: .
      dockerfile: dev.Dockerfile
```

to build from dockerfile, otherwise it will attempt to pull the image from dockerhub

# Enable hot reloading

hot reloading doesn't work when running from a container, but file changes are reflecting in /usr/src/app
https://github.com/facebook/create-react-app/issues/10253
This is likely a windows or WSL2 issue

## <b>Work Around</b>

Add `CHOKIDAR_USEPOLLING=true` in package.json, otherwise nodemon won't reload on changes.
`"dev": "CHOKIDAR_USEPOLLING=true nodemon ./bin/www"`

Refresh once and hot reloading will start working

# exercise 12.19 - connecting todo frontend and backend, using nginx as the reverse proxy

1. change backend url to `REACT_APP_BACKEND_URL=http://localhost:8080/api` from `http://localhost:3002` or whatever the backend port was

   (`nginx` image exposed port is 8080)

2. Now nginx will route requests starting with /api to the backend

```
location /api/ {
    proxy_pass http://server:3000/;
}
```

trailing / is required so nginx will proxy `/api/todos/1` to `/todos/1` on the `server` image. otherwise it will be proxied to `/api/todos/1`

# exercise 12.20 - Create production docker-compose

change `proxy_pass http://app:3000;` to `proxy_pass http://app:80;`
frontend is built and copied to nginx (port 80) folder
