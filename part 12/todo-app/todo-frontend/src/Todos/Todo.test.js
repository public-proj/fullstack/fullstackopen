import { render, screen } from '@testing-library/react';
import Todo from './Todo';

test('renders todo', () => {
  render(
    <Todo todo={{ text: 'This is a todo item for test.', done: false }} />
  );
  const linkElement = screen.getByText(/This is a todo item for test./i);
  expect(linkElement).toBeInTheDocument();
});
