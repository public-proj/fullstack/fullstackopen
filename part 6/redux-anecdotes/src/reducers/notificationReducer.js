const notificationAtStart = '';

const initialState = notificationAtStart;

export const setNotification = (content, timeInSec) => {
  return async (dispatch) => {
    dispatch({
      type: 'SET_NOTIFICATION',
      data: content,
    });

    clearTimeout();
    setTimeout(() => {
      dispatch({
        type: 'REMOVE_NOTIFICATION',
        data: '',
      });
    }, timeInSec * 1000);
  };
};

export const removeNotification = () => {
  return {
    type: 'REMOVE_NOTIFICATION',
    data: '',
  };
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_NOTIFICATION':
      return action.data;
    case 'REMOVE_NOTIFICATION':
      return '';
    default:
      return state;
  }
};

export default notificationReducer;
