import React from 'react';
import { connect } from 'react-redux';
import { addVote } from '../reducers/anecdoteReducer';
import { setNotification } from '../reducers/notificationReducer';

const Anecdote = ({ anecdote, handleClick }) => {
  return (
    <div>
      <div>{anecdote.content}</div>
      <div>
        has {anecdote.votes}
        <button onClick={handleClick}>vote</button>
      </div>
    </div>
  );
};

const Anecdotes = (props) => {
  const getAnecdoteById = (id) => {
    return props.anecdotes.filter((anecdote) => anecdote.id === id)[0];
  };

  const vote = (id) => {
    props.addVote(id);
    props.setNotification(`you voted '${getAnecdoteById(id).content}'`, 10);
  };

  return (
    <>
      {props.anecdotes.map((anecdote) => (
        <Anecdote
          key={anecdote.id}
          anecdote={anecdote}
          handleClick={() => vote(anecdote.id)}
        />
      ))}
    </>
  );
};

const mapStateToProps = (state) => {
  if (state.filter === 'ALL') {
    return {
      anecdotes: state.anecdotes,
    };
  }

  return {
    anecdotes: state.anecdotes.filter((anecdote) =>
      anecdote.content.toLowerCase().includes(state.filter.toLowerCase())
    ),
  };
};

const mapDispatchToProps = {
  addVote,
  setNotification,
};

const ConnectedAnecdotes = connect(
  mapStateToProps,
  mapDispatchToProps
)(Anecdotes);
export default ConnectedAnecdotes;
