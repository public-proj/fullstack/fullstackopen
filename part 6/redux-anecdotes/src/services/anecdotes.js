import axios from 'axios';

const baseUrl = 'http://localhost:3006/anecdotes';

const getAll = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

const getId = () => (100000 * Math.random()).toFixed(0);

const createNew = async (content) => {
  if (content) {
    const object = { content: content, id: getId(), votes: 0 };
    const response = await axios.post(baseUrl, object);
    return response.data;
  }
  return { error: 'content is empty' };
};

const addVote = async (id) => {
  const { data: anecdoteToUpdate } = await axios.get(`${baseUrl}/${id}`);
  const updatedAnecdote = {
    ...anecdoteToUpdate,
    votes: anecdoteToUpdate.votes + 1,
  };
  const response = await axios.put(`${baseUrl}/${id}`, updatedAnecdote);
  return response.data;
};

export default { getAll, createNew, addVote };
