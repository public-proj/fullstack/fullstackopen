const router = require('express').Router()
const {Blog} = require('../models')

const blogFinder = async (req, res, next) => {
    req.blog = await Blog.findByPk(req.params.id)
    next()
}

router.get('/', async (req, res) => {
    const blogs = await Blog.findAll()
    blogs.forEach(blog => {
        console.log(`${blog.author}: '${blog.title}', ${blog.likes} likes`)
    })
    res.json(blogs)
})

router.get('/:id', blogFinder, async (req, res) => {
    if (req.blog) {
        res.json(req.blog)
    } else {
        throw Error("Blog not found")
    }
})

router.post('/', async (req, res) => {
        const blog = await Blog.create(req.body)
        if (blog) {
            res.status(201).json(blog)
        }
    }
)

router.delete('/:id', blogFinder, async (req, res) => {
    if (req.blog) {
        await req.blog.destroy()
        res.status(204).send(`blog id ${req.params.id} deleted.`)
    }
})

router.put('/:id', blogFinder, async (req, res) => {
    req.blog.title = req.body.title
    req.blog.url = req.body.url
    req.blog.author = req.body.author
    if (req.blog) {
        await req.blog.save()
        res.status(200).json(req.blog)
    } else {
        throw Error("Blog not found")
    }
})

//TODO: check error handling here
router.put('/likes/:id', blogFinder, async (req, res) => {
    if (req.blog && req.body.likes) {
        req.blog.likes = req.body.likes
        await req.blog.save()
        res.json(req.blog)
    } else {
        res.status(404).end()
    }
})

module.exports = router