# Postgres setup with docker-compose
[PostgreSQL with Docker-Compose](https://zhao-li.medium.com/getting-started-with-postgresql-using-docker-compose-34d6b808c47c#:~:text=Start%20the%20Database,to%20bring%20up%20the%20database.&text=database_1%20%7C%20The%20files%20belonging%20to,also%20own%20the%20server%20process.&text=Docker%2DCompose%20will,time%20you%20run%20this%20command.)

Login with `psql -U postgres -d blogs` in the container

### Creating table for blogs
CREATE TABLE blogs (
    id SERIAL PRIMARY KEY,
    author text,
    url text NOT NULL,
    title text NOT NULL,
    likes integer DEFAULT 0
);

### Error Handling with [express-async-errors](https://github.com/davidbanham/express-async-errors)
1. utils/middleware.js - add error handler function
2. index.js - app.use(errorHandler)
3. throw errors
