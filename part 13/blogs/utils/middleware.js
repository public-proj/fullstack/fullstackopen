const errorHandler = (error, request, response, next) => {

    if (error.name === 'SequelizeValidationError'){
        return response.status(400).send({error: error.errors.map(e=>e.message)})
    }

    if (error.name === 'SequelizeDatabaseError'){
        return response.status(400).send({error: 'bad data'})
    }

    return response.status(400).send({error:error.message})
    next(error)
}

module.exports = {
    errorHandler
}