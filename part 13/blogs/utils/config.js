require('dotenv').config()

const dbUsername = process.env.POSTGRES_USER
const dbPassword = process.env.POSTGRES_PASSWORD
const dbName = process.env.POSTGRES_DB
const dbHost = process.env.POSTGRES_HOST

module.exports = {
    DATABASE_URL:`postgres://${dbUsername}:${dbPassword}@${dbHost}/${dbName}`,
    PORT: process.env.PORT || 3002
}
