const mongoose = require('mongoose');
const supertest = require('supertest');
const bcrypt = require('bcrypt');
const helper = require('./test_helper');
const app = require('../app');

const api = supertest(app);

const Blog = require('../models/blog');
const User = require('../models/user');

beforeEach(async () => {
  await Blog.deleteMany({});

  for (let blog of helper.initialBlogs) {
    let blogObject = new Blog(blog);
    await blogObject.save();
  }
});
describe('when there is initally some blogs saved', () => {
  test('all blogs are returned', async () => {
    const response = await helper.blogsInDb();
    expect(response).toHaveLength(helper.initialBlogs.length);
  });

  test('a specific blog is within the returned blogs', async () => {
    const response = await helper.blogsInDb();

    const titles = response.map((r) => r.title);
    expect(titles).toContain('React patterns');
  });

  test('blogs are returned as json', async () => {
    await api
      .get('/api/blogs')
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });
});

describe('addition of a new blog', () => {
  //4.10
  test('a valid blog can be added', async () => {
    const newBlog = {
      title: 'Valid Blog Test',
      author: 'Michael Chan 1',
      url: 'https://reactpatterns.com/1',
      likes: 7,
    };

    await api
      .post('/api/blogs')
      .send(newBlog)
      .expect(201)
      .expect('Content-Type', /application\/json/);

    const response = await helper.blogsInDb();
    expect(response).toHaveLength(helper.initialBlogs.length + 1);

    const titles = response.map((r) => r.title);
    expect(titles).toContain('Valid Blog Test');
  });

  // 4.12
  test('blog without title is not added', async () => {
    const newBlog = {
      author: 'Michael Chan 1',
      url: 'https://reactpatterns.com/1',
      likes: 7,
    };

    await api.post('/api/blogs').send(newBlog).expect(400);

    const response = await helper.blogsInDb();

    expect(response).toHaveLength(helper.initialBlogs.length);
  });

  test('blog without url is not added', async () => {
    const newBlog = {
      title: 'Valid Blog Test',
      author: 'Michael Chan 1',
      likes: 7,
    };

    await api.post('/api/blogs').send(newBlog).expect(400);

    const response = await helper.blogsInDb();

    expect(response).toHaveLength(helper.initialBlogs.length);
  });

  // 4.9
  test('verify unique identifier property of the blog post is id', async () => {
    const response = await helper.blogsInDb();
    expect(response[0].id).toBeDefined();
  });

  // 4.11
  test('likes is default to 0 if value is missing', async () => {
    const newBlog = {
      title: 'Valid Blog Test',
      author: 'Michael Chan 1',
      url: 'https://reactpatterns.com/1',
    };

    const addedBlog = await api.post('/api/blogs').send(newBlog);
    const response = await helper.blogsInDb();
    expect(addedBlog.body.likes).toBe(0);
  });
});

// 4.13 get blog by ID test
describe('viewing a specific blog', () => {
  test('succeeds with a valid id', async () => {
    const blogsAtStart = await helper.blogsInDb();

    const blogToView = blogsAtStart[0];

    const resultBlog = await api
      .get(`/api/blogs/${blogToView.id}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(resultBlog.body).toEqual(blogToView);
  });

  test('fails with statuscode 404 if blog does not exist', async () => {
    const validNonexistingId = await helper.nonExistingId();

    await api.get(`/api/blogs/${validNonexistingId}`).expect(404);
  });

  test('fails with statuscode 400 if id is invalid', async () => {
    const invalidId = '23423fgwsagsadgsagasdg';

    await api.get(`/api/blogs/${invalidId}`).expect(400);
  });
});

// 4.13 delete a blog
describe('deletion of a blog', () => {
  test('succeeds with status code 204 if id is valid', async () => {
    const blogsAtStart = await helper.blogsInDb();
    const blogsToDelete = blogsAtStart[0];

    await api.delete(`/api/blogs/${blogsToDelete.id}`).expect(204);

    const blogsAtEnd = await helper.blogsInDb();

    expect(blogsAtEnd.length).toBe(helper.initialBlogs.length - 1);

    const titles = blogsAtEnd.map((r) => r.title);

    expect(titles).not.toContain(blogsToDelete.title);
  });
});

// 4.14 update a blog
describe('update of a blog', () => {
  test('blog updates sucessfully', async () => {
    const blogsAtStart = await helper.blogsInDb();
    const blogToUpdate = blogsAtStart[0];
    const updatedBlog = { ...blogToUpdate, title: 'Updated Title' };

    await api.put(`/api/blogs/${updatedBlog.id}`).send(updatedBlog);

    const blogsAtEnd = await helper.blogsInDb();

    const titles = blogsAtEnd.map((r) => r.title);

    expect(titles).toContain(updatedBlog.title);
  });
});

afterAll(() => {
  mongoose.connection.close();
});
