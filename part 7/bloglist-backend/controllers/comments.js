const commentsRouter = require('express').Router();
const Comment = require('../models/comment');

commentsRouter.get('/', async (request, response) => {
  const comments = await Comment.find({});
  response.json(comments);
});

commentsRouter.post('/', async (request, response, next) => {
  try {
    const comment = new Comment({
      content: request.body.content,
      blog: request.body.blogId,
    });

    const savedComment = await comment.save();
    response.status(201).json(savedComment);
  } catch (error) {
    next(error);
  }
});

commentsRouter.get('/:id', async (request, response) => {
  console.log(request.params.id);
  const comment = await Comment.findById(request.params.id);

  if (comment) {
    response.json(comment.toJSON());
  } else {
    response.status(404).json({ error: 'comment not found' });
  }
});

commentsRouter.get('/byBlogId/:blogId', async (request, response) => {
  const comments = await Comment.find({ blog: request.params.blogId });

  if (comments) {
    response.json(comments);
  } else {
    response.status(404).json({ error: 'comments not found' });
  }
});

module.exports = commentsRouter;
