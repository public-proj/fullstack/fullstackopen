const usersRouter = require('express').Router();
const User = require('../models/user');

const bcrypt = require('bcrypt');

usersRouter.post('/', async (request, response) => {
  const body = request.body;

  const saltRounds = 10;
  const passwordHash = await bcrypt.hash(body.password, saltRounds);

  // TODO: check password length is >3
  if (body.password.length < 3) {
    response.status(400);
    response.json({
      success: 'false',
      error: 'password is shorter than the minimum allowed length (3)',
    });
  }

  const user = new User({
    username: body.username,
    name: body.name,
    passwordHash,
  });

  const savedUser = await user.save();

  response.json(savedUser);
});

usersRouter.get('/', async (request, response) => {
  const users = await User.find({});
  response.json(users);
});

module.exports = usersRouter;
