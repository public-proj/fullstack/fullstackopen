# Issues encountered:

## 1. TypeError: 0 is read-only

using redux toolkit changed the app to strict mode where arrays are 'freezed' and not sortable
solution: blogs.sort() => [...blogs.sort()]

https://stackoverflow.com/questions/53420055/error-while-sorting-array-of-objects-cannot-assign-to-read-only-property-2-of

## 2. When fetching from API in redux toolkit slice,

we need to use extraReducers, and createAsyncThunk

https://redux-toolkit.js.org/api/createAsyncThunk
https://www.youtube.com/watch?v=xtD4YMKWI7w&ab_channel=Rowadz

## 3. When users is initially null in the store the app will give an error because its null

a. set initial state to [] instead of null, or
b. if users === null return

```
if (!user) return <h1>Loading</h1>;

// this will load when user is available
  return (
      <h2>single user - {user.name}</h2>
  );
};
```

## 4. When going to a new page with <a> redux states are cleared

use <Link> instead of <a>

## 5. Redux states are cleard on reload

## 6. Rendered more hooks than during previous render

position of useEffect is important
putting useEffect after const blog... gives this error

BlogView.js

```
useEffect(() => {
    commentService
      .getCommentsByBlogId(id)
      .then((comments) => setComments(comments));
  }, []);

  const blog = blogs.filter((blog) => blog.id === id)[0];
  if (!blog) return <h1>Loading...</h1>;
```
