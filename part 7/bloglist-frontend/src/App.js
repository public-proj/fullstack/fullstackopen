import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom';
import BlogForm from './components/Blog/BlogForm';
import LoginForm from './components/LoginForm';
import Notification from './components/Notification/Notification';
import Nav from './components/Nav';
import { setSignedInUser } from './components/User/userSlice';
import { setNotification } from './components/Notification/notificationSlice';
import {
  fetchAllBlogs,
  setBlogs,
  postBlog,
  deleteBlog,
} from './components/Blog/blogSlice';
import Togglable from './features/Togglable';
import blogService from './services/blogs';
import loginService from './services/login';
import Users from './components/User/Users';
import User from './components/User/User';
import BlogView from './components/Blog/BlogView';

const App = () => {
  //const [blogs, setBlogs] = useState([]);
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  // const [user, setUser] = useState(null);
  // const [message, setMessage] = useState({ message: null, type: 'success' });
  // const [newBlog, setNewBlog] = useState({ title: '', author: '', url: '' });

  const dispatch = useDispatch();
  const blogFormRef = useRef();

  const blogs = useSelector((state) => state.blogs.blogs);
  const user = useSelector((state) => state.user.signedInUser);

  useEffect(() => {
    dispatch(fetchAllBlogs());
  }, [dispatch]); // refetch blogs after every dispatch.

  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem('loggedBlogAppUser');
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON);
      dispatch(setSignedInUser(user));
      blogService.setToken(user.token);
    }
  }, []);

  /*   const handleLogout = (event) => {
    event.preventDefault();
    window.localStorage.removeItem('loggedBlogAppUser');
    blogService.setToken(null);
    window.location.reload();
  }; */

  const handleLogin = async (event) => {
    event.preventDefault();
    // console.log('logging in with', username, password);

    try {
      const user = await loginService.login({ username, password });
      window.localStorage.setItem('loggedBlogAppUser', JSON.stringify(user));
      blogService.setToken(user.token);
      dispatch(setSignedInUser(user));
      setUserName('');
      setPassword('');
    } catch (exception) {
      dispatch(
        setNotification({
          message: 'Wrong username or password',
          type: 'danger',
        })
      );
    }
  };

  const handleDeleteBlog = async (id, title, author) => {
    if (window.confirm(`Remove blog ${title} by ${author}`)) {
      try {
        dispatch(deleteBlog(id));
        dispatch(setNotification({ message: 'Blog Deleted' }));
      } catch (exception) {
        dispatch(
          setNotification({
            message: `Cannot delete blog: ${exception}`,
            type: 'danger',
          })
        );
      }

      dispatch(setBlogs(blogs.filter((b) => b.id !== id)));
    }
  };

  const loginForm = () => {
    return (
      <Togglable buttonLabel="Log In">
        <LoginForm
          username={username}
          password={password}
          handleUsernameChange={({ target }) => setUserName(target.value)}
          handlePasswordChange={({ target }) => setPassword(target.value)}
          handleSubmit={handleLogin}
        />
      </Togglable>
    );
  };

  const createBlog = async (blogObject) => {
    try {
      dispatch(postBlog(blogObject));
      dispatch(
        setNotification({
          message: `A new blog ${blogObject.title} by ${blogObject.author} added`,
          type: 'success',
        })
      );
      blogFormRef.current.toggleVisibility();
    } catch (exception) {
      dispatch(
        setNotification({
          message: `Cannot save blog: ${exception}`,
          type: 'danger',
        })
      );
    }
  };

  const renderDeleteButton = (blog) => {
    if (user.id === blog.user.id) {
      return (
        <button
          className="delete-button btn btn-outline-danger ml-2"
          onClick={() => handleDeleteBlog(blog.id, blog.title, blog.author)}
        >
          Delete
        </button>
      );
    }
  };

  const Blogs = () => {
    return (
      <>
        <Togglable buttonLabel="Create New Blog" ref={blogFormRef}>
          <BlogForm createBlog={createBlog} />
        </Togglable>
        <div className="mt-2">
          <ul className="list-group">
            {[...blogs]
              .sort((a, b) => b.likes - a.likes)
              .map((blog) => (
                <li key={blog.id} className="list-group-item">
                  <Link to={`blogs/${blog.id}`} key={blog.id} className="mr-4">
                    {blog.title}
                  </Link>
                  {renderDeleteButton(blog)}
                  <br />
                </li>
              ))}
          </ul>
        </div>
      </>
    );
  };

  return (
    <Router>
      <div>
        <Nav />
        <Notification />
        <h2 className="display-4">Blogs</h2>
        {user === null ? (
          loginForm()
        ) : (
          <div>
            <Switch>
              <Route exact path="/" component={Blogs} />
              <Route path="/users/:id" component={User} />
              <Route path="/users" component={Users} />
              <Route path="/blogs/:id" component={BlogView} />
            </Switch>
          </div>
        )}
      </div>
    </Router>
  );
};

export default App;
