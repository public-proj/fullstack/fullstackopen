import axios from 'axios';
import config from '../utils/config';

const baseUrl = `${config.API_BASEURL}/api/users`;

const getAllUsers = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

export default { getAllUsers };
