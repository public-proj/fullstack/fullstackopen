import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { likeBlog } from './blogSlice';
import commentService from '../../services/comments';

const BlogView = () => {
  const [comments, setComments] = useState([]);
  const [commentInput, setCommentInput] = useState('');
  const dispatch = useDispatch();
  const blogs = useSelector((state) => state.blogs.blogs);
  const id = useParams().id;

  useEffect(() => {
    commentService
      .getCommentsByBlogId(id)
      .then((comments) => setComments(comments));
  }, []);

  const blog = blogs.filter((blog) => blog.id === id)[0];
  if (!blog) return <h1>Loading...</h1>;

  const handleAddComment = async () => {
    const savedComment = await commentService.postComment(id, commentInput);
    setComments([...comments, savedComment]);
    setCommentInput('');
  };

  return (
    <>
      <h2 className="border border-info text-info pl-4 py-2 my-4">
        {blog.title}
      </h2>
      <a href={blog.url}>{blog.url}</a>
      <br />
      <br />
      {blog.likes} likes
      <button
        className="btn btn-outline-success ml-2"
        onClick={() => dispatch(likeBlog(blog.id))}
      >
        Like
      </button>
      <br />
      added by {blog.author}
      <br />
      <br />
      <h2>comments</h2>
      <div className="input-group">
        <input
          value={commentInput}
          onChange={(e) => setCommentInput(e.target.value)}
          className="form-control"
        ></input>
        <button
          onClick={handleAddComment}
          className="btn btn-outline-primary ml-2"
        >
          {' '}
          add comment
        </button>
      </div>
      <ul className="list-group-flush">
        {comments.map((comment) => (
          <li className="list-group-item" key={comment.id}>
            {comment.content}
          </li>
        ))}
      </ul>
    </>
  );
};

export default BlogView;
