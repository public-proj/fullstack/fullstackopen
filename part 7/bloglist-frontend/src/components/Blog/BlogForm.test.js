import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BlogFrom from './BlogForm';

test('form calls the event handler it received as props with the right details when a new blog is created', () => {
  const createBlog = jest.fn();

  const component = render(<BlogFrom createBlog={createBlog} />);

  const title = component.container.querySelector('#title');
  const author = component.container.querySelector('#author');
  const url = component.container.querySelector('#url');
  const form = component.container.querySelector('form');

  fireEvent.change(title, {
    target: { value: 'This is the blog title' },
  });

  fireEvent.change(author, {
    target: { value: 'This is the blog author' },
  });

  fireEvent.change(url, {
    target: { value: 'This is the blog url' },
  });

  fireEvent.submit(form);

  expect(createBlog.mock.calls).toHaveLength(1);
  expect(createBlog.mock.calls[0][0].title).toBe('This is the blog title');
  expect(createBlog.mock.calls[0][0].author).toBe('This is the blog author');
  expect(createBlog.mock.calls[0][0].url).toBe('This is the blog url');
});
