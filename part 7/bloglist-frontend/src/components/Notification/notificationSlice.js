import { createSlice } from '@reduxjs/toolkit';

export const notificationSlice = createSlice({
  name: 'notification',
  initialState: {
    message: null,
    type: 'success',
  },
  reducers: {
    setNotification: (state, action) => {
      const { message, type } = action.payload;
      state.message = message;
      state.type = type;
    },
    dismissNotification: (state) => {
      state.message = null;
      state.type = null;
    },
  },
});

// action creators are generated for each reducer function
export const { setNotification, dismissNotification } =
  notificationSlice.actions;

export default notificationSlice.reducer;
