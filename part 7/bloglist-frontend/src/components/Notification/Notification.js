import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { dismissNotification } from './notificationSlice';

const Notification = () => {
  const message = useSelector((state) => state.notification.message);
  const type = useSelector((state) => state.notification.type) || 'success';
  const dispatch = useDispatch();

  if (message === null) {
    return null;
  }
  return (
    <div className={`d-flex justify-content-between alert alert-${type}`}>
      <div id="notification">Notification: {message}</div>
      <button
        className={`btn btn-outline-${type}`}
        onClick={() => dispatch(dismissNotification())}
      >
        X
      </button>
    </div>
  );
};

export default Notification;

/* import React from 'react';

const Notification = ({ message, type, dismissMessage }) => {
  if (message === null) {
    return null;
  }
  return (
    <div className={`d-flex justify-content-between alert alert-${type}`}>
      <div id="notification">{message}</div>
      <button className={`btn btn-outline-${type}`} onClick={dismissMessage}>
        X
      </button>
    </div>
  );
};

export default Notification;
 */
