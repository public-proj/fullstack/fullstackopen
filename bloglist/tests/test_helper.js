const Blog = require('../models/blog');
const User = require('../models/user');

const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.SECRET;

const initialBlogs = [
  {
    title: 'React patterns',
    author: 'Michael Chan',
    url: 'https://reactpatterns.com/',
    likes: 7,
  },
  {
    title: 'Go To Statement Considered Harmful',
    author: 'Edsger W. Dijkstra',
    url:
      'http://www.u.arizona.edu/~rubinson/copyright_violations/Go_To_Considered_Harmful.html',
    likes: 5,
  },
];

/*
const sampleUsers = [
  {
    username: 'pmurphy',
    name: 'Patrick',
    password: 'patrick',
  },
  {
    username: 'bmurphy',
    name: 'Brandon',
    password: 'brandon',
  },
];
*/

const blogsInDb = async () => {
  const blogs = await Blog.find({});
  return blogs.map((blog) => blog.toJSON());
};

const nonExistingId = async () => {
  const blog = new Blog({ title: 'Will remove soon', url: 'url' });
  await blog.save();
  await blog.remove();

  return blog._id.toString();
};

const usersInDb = async () => {
  const users = await User.find({});
  return users.map((u) => u.toJSON());
};

/*
const getToken = (user) => {
  return jwt.sign(user, JWT_SECRET);
};

const getDecodedToken = (token) => {
  return jwt.verify(token, JWT_SECRET);
};
*/

module.exports = {
  initialBlogs,
  blogsInDb,
  nonExistingId,
  usersInDb,
};
