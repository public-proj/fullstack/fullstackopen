ESLint Error:
No files matching the pattern "'./**/*.{js,jsx}'" were found.

Fix: 
change

```
"lint": "eslint '**/*.{ts,tsx}'"
```

```
"lint": "eslint \"**/*.{ts,tsx}\""
```

Cause: 
The --no-error-on-unmatched-pattern flag was added in v6.8.0 of ESLint.