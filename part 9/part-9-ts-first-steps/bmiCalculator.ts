interface BmiValues {
  height: number;
  mass: number;
}

const parseArgs = (args: Array<string>): BmiValues => {
  if (args.length < 4) throw new Error('Not enough arguments');
  if (args.length > 4) throw new Error('Too many arguments');

  if (!isNaN(Number(args[2])) && !isNaN(Number(args[3]))) {
    return {
      height: Number(args[2]),
      mass: Number(args[3]),
    };
  } else {
    throw new Error('Provided values were not numbers!');
  }
};

export const calculateBmi = (height: number, mass: number): string => {
  const bmi = mass / Math.pow(height / 100, 2);
  if (bmi < 18.5) {
    return 'Underweight';
  } else if (bmi < 25.0) {
    return 'Normal (healthy weight)';
  } else if (bmi < 30.0) {
    return 'Overweight';
  } else {
    return 'Obese';
  }
};

try {
  const { height, mass } = parseArgs(process.argv);
  console.log(calculateBmi(height, mass));
} catch (e: unknown) {
  if (e instanceof Error) {
    console.log(`Error, something bad happened, message: ${e.message}`);
  }
}

//console.log(calculateBmi(180, 74));
