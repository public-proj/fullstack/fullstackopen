export {};
interface TrainingValues {
  periodLength: number;
  trainingDays: number;
  success: boolean;
  rating: number;
  ratingDescription: string;
  target: number;
  average: number;
}

interface TrainingInputValues {
  target: number;
  hours: Array<number>;
}

const parseArgs = (args: Array<string>): TrainingInputValues => {
  if (args.length < 4) throw new Error('Not enough arguments');

  // TODO: Add more validations

  return {
    target: Number(args[2]),
    hours: args.slice(3).map((hour) => Number(hour)),
  };
};

export const calculateExercises = (
  target: number,
  hours: number[]
): TrainingValues => {
  return {
    periodLength: hours.length,
    trainingDays: hours.filter((hour) => hour !== 0).length,
    success: false,
    rating: 2,
    ratingDescription: 'not too bad but could be better',
    target: target,
    average: hours.reduce((a, b) => a + b) / hours.length,
  };
};

// console.log(calculateExercises([3, 0, 2, 4.5, 0, 3, 1], 2));
try {
  const { target, hours } = parseArgs(process.argv);
  console.log(calculateExercises(target, hours));
} catch (e: unknown) {
  if (e instanceof Error) {
    console.log(`Error, something bad happened, message: ${e.message}`);
  }
}
