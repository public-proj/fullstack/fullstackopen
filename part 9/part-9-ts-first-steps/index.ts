import express from 'express';
import { calculateBmi } from './bmiCalculator';
import { calculateExercises } from './exerciseCalculator';

const app = express();
app.use(express.json());

// exercise 9.4
app.get('/hello', (_req, res) => {
  res.send('Hello Full Stack');
});

// exercise 9.5
// url: http://localhost:3002/bmi?height=180&weight=72
/*
    response: {
    weight: 72,
    height: 180,
    bmi: "Normal (healthy weight)"
    }
*/
app.get('/bmi', (req, res) => {
  const height = Number(req.query.height);
  const weight = Number(req.query.weight);

  if (isNaN(height) || isNaN(weight)) {
    res.status(400).send({ error: 'malformatted parameters' });
  } else {
    res.send({
      weight: weight,
      height: height,
      bmi: calculateBmi(height, weight),
    });
  }
});

app.post('/exercises', (req, res) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const { body } = req;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const { daily_exercises: hours, target } = body;

  if (!hours || !target) {
    return res.status(400).send({ error: 'parameters missing' });
  }
  // check if hours is an array of numbers
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const isArrayOfNumbers = (arr: any): boolean => {
    return Array.isArray(arr) && arr.every((item) => !isNaN(Number(item)));
  };

  if (isNaN(target) || !isArrayOfNumbers(hours)) {
    return res.status(400).send({
      error: 'malformatted parameters',
    });
  } else {
    return res.send(calculateExercises(target, hours));
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
