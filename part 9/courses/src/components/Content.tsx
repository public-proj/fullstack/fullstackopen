import React from 'react';
import { ContentProps } from '../types/Courses';
import Part from './Part';

const Content = ({ courseParts }: ContentProps) => {
  return (
    <>
      {courseParts.map((part) => (
        <p key={part.name}>
          <Part part={part} />
        </p>
      ))}

      {/* <p>
        {courseParts[0].name} {courseParts[0].exerciseCount}
      </p>
      <p>
        {courseParts[1].name} {courseParts[1].exerciseCount}
      </p>
      <p>
        {courseParts[2].name} {courseParts[2].exerciseCount}
      </p>
      <p>
        {courseParts[3].name} {courseParts[3].exerciseCount}
      </p>
      <p>
        {courseParts[4].name} {courseParts[4].exerciseCount}
      </p> */}
    </>
  );
};

export default Content;
