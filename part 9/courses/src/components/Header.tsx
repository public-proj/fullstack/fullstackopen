import React from 'react';
import { HeaderProps } from '../types/Courses';

const Header = ({ courseName }: HeaderProps) => {
  return <h1>{courseName}</h1>;
};

export default Header;
