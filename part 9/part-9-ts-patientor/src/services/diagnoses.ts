import diagnosesData from '../../data/diagnoses';

import { Diagnosis } from '../types';

const diagnoses: Array<Diagnosis> = diagnosesData;

const getEntries = () => {
  return diagnoses;
};

const addEntry = () => {
  return null;
};

const getDiagnosisById = (code: string) => {
  return diagnoses.filter((dx) => dx.code === code)[0];
};

export default {
  getEntries,
  addEntry,
  getDiagnosisById,
};
