import patientData from '../../data/patients';
import { v4 as uuidv4 } from 'uuid';

import {
  NonSensitivePatient,
  NewPatient,
  Patient,
  NewEntry,
  Entry,
} from '../types';

//const patients: Array<PatientEntry> = patientData;

const getEntries = (): Patient[] => {
  return patientData;
};

const getNonSensitvePatientData = (): NonSensitivePatient[] => {
  return patientData.map(({ id, name, dateOfBirth, gender, occupation }) => ({
    id,
    name,
    dateOfBirth,
    gender,
    occupation,
  }));
};

const getPatientById = (id: String): Patient => {
  return patientData.filter((patient) => patient.id === id)[0];
};

const addPatient = (entry: NewPatient): Patient => {
  const newPatientEntry = {
    id: uuidv4(),
    ...entry,
  };

  patientData.push(newPatientEntry);
  return newPatientEntry;
};

const addEntryByPatientId = (id: String, entry: NewEntry): Entry => {
  const newEntryWithId = {
    id: uuidv4(),
    ...entry,
  };
  patientData
    .filter((patient) => patient.id === id)[0]
    .entries?.push(newEntryWithId);
  return newEntryWithId;
};

export default {
  getEntries,
  addPatient,
  getNonSensitvePatientData,
  getPatientById,
  addEntryByPatientId,
};
