import express from 'express';
import patientsService from '../services/patients';
import toNewPatientEntry, { toNewEntry } from '../utils';

const router = express.Router();

router.get('/', (_req, res) => {
  res.send(patientsService.getNonSensitvePatientData());
});

router.post('/', (req, res) => {
  try {
    const newPatientEntry = toNewPatientEntry(req.body);
    const addedPatientEntry = patientsService.addPatient(newPatientEntry);
    res.json(addedPatientEntry);
  } catch (e) {
    res.status(400).send(e.message);
  }
});

router.get('/:id', (req, res) => {
  try {
    const foundPatient = patientsService.getPatientById(req.params.id);
    if (foundPatient) {
      res.status(200).send(foundPatient);
    } else {
      res.status(404).send('Patient not found');
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

router.post('/:id/entries', (req, res) => {
  try {
    const newEntry = toNewEntry(req.body);
    const addedEntry = patientsService.addEntryByPatientId(
      req.params.id,
      newEntry
    );
    res.json(addedEntry);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default router;
