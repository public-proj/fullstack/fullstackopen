import express from 'express';
import diagnosesService from '../services/diagnoses';

const router = express.Router();

router.get('/', (_req, res) => {
  res.send(diagnosesService.getEntries());
});

router.post('/', (_req, res) => {
  res.send('saving a diagnosis');
});

router.get('/code/:code', (req, res) => {
  try {
    const found = diagnosesService.getDiagnosisById(req.params.code);
    if (found) {
      res.status(200).send(found);
    } else {
      res.status(404).send(`DiagnosisCode ${req.params.code} not found`);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

export default router;
