import {
  Gender,
  NewEntry,
  BaseEntry,
  NewPatient,
  HealthCheckRating,
  Discharge,
  SickLeave,
} from './types';

const isString = (text: unknown): text is string => {
  return typeof text === 'string' || text instanceof String;
};

const parseName = (name: unknown): string => {
  if (!name || !isString(name)) {
    throw new Error(`Incorrect or missing name: ${name}`);
  }
  return name;
};

const isDate = (date: string): boolean => {
  return Boolean(Date.parse(date));
};

const parseDate = (date: unknown): string => {
  if (!date || !isString(date) || !isDate(date)) {
    throw new Error(`Incorrect or missing date of birth: ${date}`);
  }
  return date;
};

const parseSSN = (ssn: unknown): string => {
  // TODO: could add ssn validation with regex
  if (!ssn || !isString(ssn)) {
    throw new Error(`Incorrect or missing ssn: ${ssn}`);
  }
  return ssn;
};

const isGender = (gender: any): gender is Gender => {
  return Object.values(Gender).includes(gender);
};

const parseGender = (gender: unknown): Gender => {
  if (!gender || !isGender(gender)) {
    throw new Error(`Incorrect or missing genderr: ${gender}`);
  }
  return gender;
};

const parseOccupation = (occupation: unknown): string => {
  if (!occupation || !isString(occupation)) {
    throw new Error(`Incorrect or missing occupation: ${occupation}`);
  }
  return occupation;
};

const parseDescription = (description: unknown): string => {
  if (!description || !isString(description)) {
    throw new Error(`Incorrect or missing description: ${description}`);
  }
  return description;
};

const parseSpecialist = (specialist: unknown): string => {
  if (!specialist || !isString(specialist)) {
    throw new Error(`Incorrect or missing specialist: ${specialist}`);
  }
  return specialist;
};

const parseDiagnosisCodes = (diagnosisCodes: any): string[] => {
  if (!diagnosisCodes) return diagnosisCodes;

  if (!Array.isArray(diagnosisCodes)) {
    throw new Error('Incorrect diagnosisCode format');
  }

  return diagnosisCodes;
};

const parseHealthCheckRating = (healthCheckRating: any): HealthCheckRating => {
  if (
    (healthCheckRating !== 0 && !healthCheckRating) ||
    !(healthCheckRating in HealthCheckRating)
  ) {
    throw new Error('Incorrect HealthCheckRating, missing or incorrect format');
  }
  return healthCheckRating;
};

const parseDischarge = (discharge: any): Discharge => {
  if (!discharge.date || !discharge.criteria) {
    throw new Error('Missing discharge date or criteria');
  }

  if (!isString(discharge.criteria)) {
    throw new Error('Discharge criteria - wrong format');
  }

  return {
    date: parseDate(discharge.date),
    criteria: discharge.criteria,
  };
};

const parseEmployerName = (employerName: unknown): string => {
  if (!employerName || !isString(employerName)) {
    throw new Error(`Incorrect or missing employer name: ${employerName}`);
  }
  return employerName;
};

const parseSickLeave = (sickLeave: any): SickLeave => {
  if (!sickLeave) return sickLeave;
  if (!sickLeave.startDate) {
    throw new Error(`Missing sickLeave start date`);
  }
  if (!sickLeave.endDate) {
    throw new Error(`Missing sickLeave end date`);
  }
  return {
    startDate: parseDate(sickLeave.startDate),
    endDate: parseDate(sickLeave.endDate),
  };
};

const assertNever = (value: never): never => {
  throw new Error(`Unhandled entry type: ${JSON.stringify(value)}`);
};

type Fields = {
  name: unknown;
  dateOfBirth: unknown;
  ssn: unknown;
  gender: unknown;
  occupation: unknown;
};

const toNewPatientEntry = ({
  name,
  dateOfBirth,
  ssn,
  gender,
  occupation,
}: Fields): NewPatient => {
  const newEntry: NewPatient = {
    name: parseName(name),
    dateOfBirth: parseDate(dateOfBirth),
    ssn: parseSSN(ssn),
    gender: parseGender(gender),
    occupation: parseOccupation(occupation),
  };
  return newEntry;
};

export const toNewEntry = (newEntry: any): NewEntry => {
  // 1. check the entry type, if invalid, return error
  const isValidEntry =
    newEntry.type === 'HealthCheck' ||
    newEntry.type === 'Hospital' ||
    newEntry.type === 'OccupationalHealthcare';

  if (!isValidEntry) {
    throw new Error('Entry not valid (invalid type).');
  }

  const parsedEntry = newEntry as NewEntry;
  // 2. set base entry stuff

  let entry: Omit<BaseEntry, 'id'> = {
    description: parseDescription(newEntry.description),
    date: parseDate(newEntry.date),
    specialist: parseSpecialist(newEntry.specialist),
    diagnosisCodes: parseDiagnosisCodes(newEntry.diagnosisCodes),
  };

  // 3. add specific entry information
  switch (parsedEntry.type) {
    case 'HealthCheck':
      return {
        ...entry,
        type: 'HealthCheck',
        healthCheckRating: parseHealthCheckRating(
          parsedEntry.healthCheckRating
        ),
      };
    case 'Hospital':
      return {
        ...entry,
        type: 'Hospital',
        discharge: parseDischarge(parsedEntry.discharge),
      };
    case 'OccupationalHealthcare':
      return {
        ...entry,
        type: 'OccupationalHealthcare',
        employerName: parseEmployerName(parsedEntry.employerName),
        sickLeave: parseSickLeave(parsedEntry.sickLeave),
      };
    default:
      return assertNever(parsedEntry);
  }
  // 4. return the entry
};

export default toNewPatientEntry;
