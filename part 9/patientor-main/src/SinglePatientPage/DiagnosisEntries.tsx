import React from 'react';
import { Card, List } from 'semantic-ui-react';
import { Entry } from '../types';
import DiagnosisCode from './DiagnosisCode';

import DiagnosisEntry from './DiagnosisEntry';
//import DiagnosisEntry from './DiagnosisEntry';

interface DiagnosisEntriesProps {
  entries: Entry[] | undefined;
}

const DiagnosisEntries = (dxEntries: DiagnosisEntriesProps) => {
  return (
    <Card.Group>
      {dxEntries.entries?.map((entry) => (
        <Card fluid key={entry.id}>
          <DiagnosisEntry entry={entry} key={entry.id} />
          {entry.diagnosisCodes ? (
            <Card.Content>
              <List>
                {entry.diagnosisCodes?.map((dxCode, i) => (
                  <div key={i}>
                    <DiagnosisCode dxCode={dxCode} />
                  </div>
                ))}
              </List>
            </Card.Content>
          ) : (
            ''
          )}
        </Card>
      ))}
    </Card.Group>
  );
};

export default DiagnosisEntries;
