import React from 'react';
import { Card, Icon } from 'semantic-ui-react';
import { HospitalEntry as HospitalEntryType } from '../types';

interface HospitalEntryProps {
  entry: HospitalEntryType;
}

const HospitalEntry = ({ entry }: HospitalEntryProps) => {
  return (
    <>
      <Card.Content style={{ backgroundColor: 'pink' }}>
        <Card.Header>
          {entry.date} <Icon name="hospital" size="large" />
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <p>{entry.description}</p>
        <p>
          Discharged: {entry.discharge.date} ({entry.discharge.criteria})
        </p>
      </Card.Content>
    </>
  );
};

export default HospitalEntry;
