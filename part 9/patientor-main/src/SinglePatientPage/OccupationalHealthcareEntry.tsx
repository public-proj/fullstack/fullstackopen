import React from 'react';
import { Card, Icon, Label, List } from 'semantic-ui-react';
import { OccupationalHealthcareEntry as OccupationalHealthcareEntryType } from '../types';

interface OccupationalHealthcareEntryProps {
  entry: OccupationalHealthcareEntryType;
}

const OccupationalHealthcareEntry = ({
  entry,
}: OccupationalHealthcareEntryProps) => {
  return (
    <>
      <Card.Content style={{ backgroundColor: '#ccfcb1' }}>
        <Card.Header>
          {entry.date} <Icon name="stethoscope" size="large" />
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <p>{entry.description}</p>
        <List>
          <List.Item>
            <Label>Specialist</Label>
            {` ${entry.specialist}`}
          </List.Item>
          <List.Item>
            <Label>Employer</Label>
            {` ${entry.employerName}`}
          </List.Item>

          {entry.sickLeave ? (
            <List.Item>
              <Label>SickLeave</Label>
              {` ${entry.sickLeave?.startDate} - ${entry.sickLeave?.endDate}`}
            </List.Item>
          ) : (
            ''
          )}
        </List>
      </Card.Content>
    </>
  );
};

export default OccupationalHealthcareEntry;
