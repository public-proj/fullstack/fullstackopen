import axios from 'axios';

import React, { useEffect, useState } from 'react';
import { List } from 'semantic-ui-react';
import { apiBaseUrl } from '../configs/constants';
//import { Entry } from '../types';

interface DiagnosisCodeProps {
  dxCode: string;
}

//TODO: will probably need to pass the whole entry instead of code
const DiagnosisCode = ({ dxCode }: DiagnosisCodeProps) => {
  const [codeName, setCodeName] = useState('');
  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await axios.get(
          `${apiBaseUrl}/diagnoses/code/${dxCode}`
        );
        setCodeName(result.data.name);
      } catch (e) {
        console.log(e.message);
      }
    };
    void fetchData();
  });

  return (
    <List.Item>
      {dxCode} - {codeName}
    </List.Item>
  );
};

export default DiagnosisCode;
