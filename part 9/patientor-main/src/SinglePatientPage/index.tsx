import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import {
  BaseEntry,
  HealthCheckEntry,
  HospitalEntry,
  OccupationalHealthcareEntry,
  Patient,
} from '../types';
import { Button, Container, Divider, Icon, Table } from 'semantic-ui-react';
import DiagnosisEntries from './DiagnosisEntries';
import AddPatientEntryModal from '../AddPatientEntryModal';
import { PatientEntryFormValues } from '../AddPatientEntryModal/AddPatientEntryForm';
import { apiBaseUrl } from '../configs/constants';

const SinglePatientPage = () => {
  const { patientId } = useParams<{ patientId: string }>();
  const [patient, setPatient] = useState<Patient | null>(null);
  const [modalOpen, setModalOpen] = React.useState<boolean>(false);

  const openModal = (): void => setModalOpen(true);

  const closeModal = (): void => {
    setModalOpen(false);
  };

  const submitNewPatientEntry = async (values: PatientEntryFormValues) => {
    try {
      const baseEntryData: Omit<BaseEntry, 'id'> = {
        description: values.description,
        date: values.date,
        specialist: values.specialist,
        diagnosisCodes: values.diagnosisCodes,
      };

      switch (values.type) {
        case 'HealthCheck':
          const data: Omit<HealthCheckEntry, 'id'> = {
            ...baseEntryData,
            type: 'HealthCheck',
            healthCheckRating: values.healthCheckRating,
          };

          await axios.post(`${apiBaseUrl}/patients/${patientId}/entries`, data);
          break;

        case 'Hospital':
          const hospitalEntryData: Omit<HospitalEntry, 'id'> = {
            ...baseEntryData,
            type: 'Hospital',
            discharge: {
              date: values.dischargeDate,
              criteria: values.dischargeCriteria,
            },
          };

          await axios.post(
            `${apiBaseUrl}/patients/${patientId}/entries`,
            hospitalEntryData
          );
          break;

        case 'OccupationalHealthcare':
          const ohEntryData: Omit<OccupationalHealthcareEntry, 'id'> = {
            ...baseEntryData,
            type: 'OccupationalHealthcare',
            employerName: values.employerName,
            sickLeave: {
              startDate: values.sickLeaveStart,
              endDate: values.sickLeaveEnd,
            },
          };

          if (values.sickLeaveEnd === '' || values.sickLeaveStart === '') {
            delete ohEntryData.sickLeave;
          }

          await axios.post(
            `${apiBaseUrl}/patients/${patientId}/entries`,
            ohEntryData
          );
          break;

        default:
          break;
      }
      closeModal();
      void fetchData();
    } catch (e) {
      console.error(e.response?.data || 'Unknown Error');
    }
  };

  const fetchData = async () => {
    try {
      const result = await axios.get(
        `http://localhost:3000/api/patients/${patientId}`
      );

      setPatient(result.data);
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    void fetchData();
  }, []);

  if (patient === null) {
    return <p>Loading...</p>;
  }

  return (
    <Container fluid>
      <h2>
        {patient.name}

        <Icon
          name={
            patient.gender === 'male'
              ? 'mars'
              : patient.gender === 'female'
              ? 'venus'
              : 'genderless'
          }
        />
      </h2>
      <br />
      <Table definition>
        <Table.Body>
          <Table.Row>
            <Table.Cell width={2}>SSN</Table.Cell>
            <Table.Cell>{patient.ssn}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Occupation</Table.Cell>
            <Table.Cell>{patient.occupation}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
      <br />
      <Divider horizontal>Entries</Divider>
      <AddPatientEntryModal
        modalOpen={modalOpen}
        onSubmit={submitNewPatientEntry}
        onClose={closeModal}
      />
      <Button basic color="blue" onClick={() => openModal()}>
        Add New Entry
      </Button>
      <Divider horizontal />
      <DiagnosisEntries entries={patient.entries} />
    </Container>
  );
};

export default SinglePatientPage;
