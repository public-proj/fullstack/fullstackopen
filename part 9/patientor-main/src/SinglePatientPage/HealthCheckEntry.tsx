import React from 'react';
import { Card, Icon } from 'semantic-ui-react';
import { SemanticCOLORS } from 'semantic-ui-react/dist/commonjs/generic';
import { HealthCheckEntry as HealthCheckEntryType } from '../types';

interface HealthCheckEntryProps {
  entry: HealthCheckEntryType;
}

const healthCheckRatingColors: Map<number, SemanticCOLORS> = new Map([
  [0, 'green'],
  [1, 'yellow'],
  [2, 'orange'],
  [3, 'red'],
]);

const HealthCheckEntry = ({ entry }: HealthCheckEntryProps) => {
  return (
    <>
      <Card.Content style={{ backgroundColor: '#fcf1b1' }}>
        <Card.Header>
          {entry.date}
          <Icon name="user doctor" size="large" />
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <p>{entry.description}</p>
        <Icon
          name="heart"
          color={healthCheckRatingColors.get(entry.healthCheckRating)}
        />
      </Card.Content>
    </>
  );
};

export default HealthCheckEntry;
