import React, { useState } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { Field, Formik, Form } from 'formik';

import {
  TextField,
  SelectField,
  TypeOption,
  DiagnosisSelection,
  NumberField,
} from './FormField';
import { HealthCheckRating } from '../types';
import { useStateValue } from '../state';
import { isValidDate } from '../utils';

type diagnosisTypeString =
  | 'HealthCheck'
  | 'Hospital'
  | 'OccupationalHealthcare';

//export type PatientEntryFormValues = Omit<Entry, 'id'>;
export type PatientEntryFormValues = {
  date: string;
  description: string;
  diagnosisCodes: string[];
  dischargeCriteria: string;
  dischargeDate: string;
  employerName: string;
  healthCheckRating: HealthCheckRating;
  sickLeaveEnd: string;
  sickLeaveStart: string;
  specialist: string;
  type: diagnosisTypeString;
};

interface Props {
  onSubmit: (values: PatientEntryFormValues) => void;
  onCancel: () => void;
}

const typeOptions: TypeOption[] = [
  { value: 'HealthCheck', label: 'HealthCheck' },
  { value: 'Hospital', label: 'Hospital' },
  { value: 'OccupationalHealthcare', label: 'OccupationalHealthcare' },
];

export const AddPatientEntryForm = ({ onSubmit, onCancel }: Props) => {
  const [{ diagnoses }] = useStateValue();
  const [type, setType] = useState<diagnosisTypeString>('HealthCheck');

  return (
    <Formik
      initialValues={{
        type,
        description: '',
        date: new Date().toJSON().slice(0, 10),
        specialist: '',
        diagnosisCodes: [],
        healthCheckRating: 0,
        dischargeDate: new Date().toJSON().slice(0, 10),
        dischargeCriteria: '',
        employerName: '',
        sickLeaveStart: '',
        sickLeaveEnd: '',
      }}
      onSubmit={onSubmit}
      validate={(values) => {
        const requiredError = 'Field is required';
        const dateFormateError = 'Date format must be YYYY-MM-DD';
        const errors: { [field: string]: string } = {};
        // base entry validation

        if (!values.description) {
          errors.description = requiredError;
        }
        if (!values.date) {
          errors.date = requiredError;
        } else {
          if (!isValidDate(values.date)) {
            errors.date = dateFormateError;
          }
        }

        if (!values.specialist) {
          errors.specialist = requiredError;
        }
        // Healthcheck validation
        if (type === 'HealthCheck') {
          if (!values.healthCheckRating && values.healthCheckRating !== 0) {
            errors.healthCheckRating = requiredError;
          }
        }

        if (type === 'Hospital') {
          if (!values.dischargeDate) {
            errors.dischargeDate = requiredError;
          } else {
            if (!isValidDate(values.dischargeDate)) {
              errors.dischargeDate = dateFormateError;
            }
          }
          if (!values.dischargeCriteria) {
            errors.dischargeCriteria = requiredError;
          }
        }

        if (type === 'OccupationalHealthcare') {
          if (values.sickLeaveStart && !isValidDate(values.sickLeaveStart)) {
            errors.sickLeaveStart = dateFormateError;
          }
          if (values.sickLeaveEnd && !isValidDate(values.sickLeaveEnd)) {
            errors.sickLeaveEnd = dateFormateError;
          }
        }

        return errors;
      }}
    >
      {({ isValid, dirty, setFieldValue, setFieldTouched }) => {
        return (
          <Form className="form ui">
            <SelectField
              label="Type"
              name="type"
              options={typeOptions}
              value={type}
              onChange={(e) => {
                setType(e.target.value as diagnosisTypeString);
                setFieldValue('type', e.target.value as diagnosisTypeString);
              }}
            />

            <Field
              label="Description"
              placeholder="Description"
              name="description"
              component={TextField}
            />
            <Field
              label="Date"
              placeholder="YYYY-MM-DD"
              name="date"
              component={TextField}
            />
            <Field
              label="Specialist"
              placeholder="Specialist"
              name="specialist"
              component={TextField}
            />

            <DiagnosisSelection
              setFieldValue={setFieldValue}
              setFieldTouched={setFieldTouched}
              diagnoses={Object.values(diagnoses)}
            />

            {type === 'HealthCheck' ? (
              <Field
                label="healthCheckRating"
                name="healthCheckRating"
                component={NumberField}
                min={0}
                max={3}
              />
            ) : type === 'Hospital' ? (
              <>
                <Field
                  label="Discharge Date"
                  placeholder="YYYY-MM-DD"
                  name="dischargeDate"
                  component={TextField}
                />
                <Field
                  label="Discharge Criteria"
                  placeholder="Discharge Criteria"
                  name="dischargeCriteria"
                  component={TextField}
                />
              </>
            ) : type === 'OccupationalHealthcare' ? (
              <>
                <Field
                  label="Employer's Name"
                  placeholder="Employer's Name"
                  name="employerName"
                  component={TextField}
                />
                <Field
                  label="Sick Leave Start Date"
                  placeholder="YYYY-MM-DD"
                  name="sickLeaveStart"
                  component={TextField}
                />
                <Field
                  label="Sick Leave End Date"
                  placeholder="YYYY-MM-DD"
                  name="sickLeaveEnd"
                  component={TextField}
                />
              </>
            ) : (
              ''
            )}

            <Grid>
              <Grid.Column floated="left" width={5}>
                <Button type="button" onClick={onCancel} color="red">
                  Cancel
                </Button>
              </Grid.Column>
              <Grid.Column floated="right" width={5}>
                <Button
                  type="submit"
                  floated="right"
                  color="green"
                  disabled={!dirty || !isValid}
                >
                  Add
                </Button>
              </Grid.Column>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

export default AddPatientEntryForm;
