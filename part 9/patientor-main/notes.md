### typing functional component props

With React component props, use:

```js
const Header: React.FC<{ loading: boolean }> = ({ loading }) => {
  if (loading) {
    return <header>{T`Loading...`}</header>;
  }

  return <header>{T`Harry's header`}</header>;
};
```

without

```js
const Header = ({ loading }: { loading: boolean }) => {
  if (loading) {
    return <header>{T`Loading...`}</header>;
  }

  return <header>{T`Harry's header`}</header>;
};
```

### useParams with Typescript

```js
const { patientId } = useParams<{ patientId: string }>();
```

### Invoking arrow functions without names

```js
(async () => {
  console.log(await asyncFunction());
})();
```

### ES-Lint Error: Promises must be handled appropriately or explicitly marked as ignored with the void operator

```js
void fetchData();
```

### Dictionary/Map in Typescript

https://levelup.gitconnected.com/building-type-safe-dictionaries-in-typescript-a072d750cbdf

used in HealthCheckEntry.tsx

### overwriting onChange in a Formik component

use `setFieldValue` to set values otherwise it won't update
